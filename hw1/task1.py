from re import search


def domain_name(url: str):
    return search(r'(https?://)?(www\.)?(?P<dom_name>[\w-]+)+(\.\w+)', url).group('dom_name')
