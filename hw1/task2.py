def int32_to_ip(int32: int):
    return "{}.{}.{}.{}".format(*int32.to_bytes(4, byteorder='big'))
