from math import log


def zeros(n: int):
    result = 0
    if n != 0:
        for k in range(1, int(log(n, 5))+1):
            result += n//(5**k)
    return result
