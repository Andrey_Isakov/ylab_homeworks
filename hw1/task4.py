from itertools import combinations


def bananas(s: str, word='banana'):
    res = set()
    for positions in combinations(range(len(s)), len(s)-6):
        s_list = list(s)
        for position in positions:
            s_list[position] = '-'
        new_s = ''.join(s_list)
        if new_s.replace('-', '') == word:
            res.add(new_s)
    return res
