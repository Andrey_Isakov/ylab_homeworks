from math import prod


def count_find_num(primesL: list, limit: int):
    res = [prod(primesL)]
    if res[0] <= limit:
        i = 0
        while i < len(res):
            for prime in primesL:
                if res[i]*prime <= limit:
                    res.append(res[i]*prime)
            i += 1
        res = set(res)
        return [len(res), max(res)]
    else:
        return []
